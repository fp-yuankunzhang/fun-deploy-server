# -*- coding: utf-8 -*-
from flask import current_app
from flask_script import Manager, prompt_bool

from gateway import create_app
from gateway.extensions import db
from gateway.demo.models import User

manager = Manager(create_app(config_dict={'DISABLE_ADMIN': True}))


@manager.shell
def make_shell_context():
    return dict(app=current_app)

@manager.command
def db_init():
    if not prompt_bool(
            'Caution!!!\nThis will drop the whole database '
            'and re-create it, are you sure?'):
        return

    db.drop_all()
    db.create_all()


if __name__ == '__main__':
    manager.run()