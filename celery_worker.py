#!/usr/bin/env python
import os
from gateway import create_app
from gateway.extensions import celery

app = create_app()
app.app_context().push()