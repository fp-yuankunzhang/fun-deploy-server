# -*- coding: utf-8 -*-
'''
    application.py
    ~~~~~~

    Application setup.

    :copyright: (c) 2016 FunPlus
    :license:
'''
from flask import Flask

from .extensions import db, socketio, celery
from .demo.views import demo_bp

__all__ = ['create_app']

ENABLED_BLUEPRINTS = (
    demo_bp
)


def create_app(app_name=None, config_dict=None):
    '''
    Create a new Flask application instance, and return it to the caller.
    '''
    if not app_name:
        app_name = 'gateway'

    app = Flask(app_name)

    configure_app(app, config_dict)
    configure_logging(app)
    configure_error_handlers(app)
    configure_event_hooks(app)
    configure_extensions(app)
    configure_blueprints(app, ENABLED_BLUEPRINTS)
    configure_admin(app)

    return app


def configure_app(app, config_dict):
    app.config.from_object('gateway.config.Config')

    if config_dict is not None:
        app.config.update(config_dict)

    app.config.from_envvar('APP_CONFIG', silent=True)


def configure_logging(app):
    if app.debug or app.testing:
        return


def configure_error_handlers(app):
    if app.testing:
        return

    @app.errorhandler(401)
    def unauthorized(error):
        return 'Unauthorized'

    @app.errorhandler(403)
    def forbidden(error):
        return 'Forbidden'

    @app.errorhandler(404)
    def page_not_found(error):
        return 'Page Not Found'

    @app.errorhandler(500)
    def server_error(error):
        return 'Internal Server Error'


def configure_event_hooks(app):
    pass


def configure_extensions(app):
    db.init_app(app)
    celery.init_app(app)
    socketio.init_app(app,
                      async_mode='eventlet',
                      message_queue=app.config['SOCKETIO_REDIS_URL'],
                      channel='websocket',
                      ping_interval=app.config['SOCKET_IO_PING_INTERVAL'],
                      ping_timeout=app.config['SOCKET_IO_PING_TIMEOUT'])


def configure_blueprints(app, blueprints):
    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def configure_admin(app):
    if app.config.get("DISABLE_ADMIN"):
        return

    from flask_superadmin import Admin
    from .demo.models import User

    admin = Admin(app)
    admin.register(User, session=db.session)