# -*- coding: utf-8 -*-
'''
    extensions.py
    ~~~~~~

    Extensions.

    :copyright: (c) 2016 FunPlus
    :license:
'''
from flask import has_app_context
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO
from celery import Celery

__all__ = ['db', 'socketio', 'celery']


class FlaskCelery(Celery):

    def __init__(self, *args, **kwargs):

        super(FlaskCelery, self).__init__(*args, **kwargs)
        self.patch_task()

        if 'app' in kwargs:
            self.init_app(kwargs['app'])

    def patch_task(self):
        TaskBase = self.Task
        _celery = self

        class ContextTask(TaskBase):
            abstract = True

            def __call__(self, *args, **kwargs):
                if has_app_context():
                    return TaskBase.__call__(self, *args, **kwargs)
                else:
                    with _celery.app.app_context():
                        return TaskBase.__call__(self, *args, **kwargs)

        self.Task = ContextTask

    def init_app(self, app):
        self.app = app
        self.config_from_object(app.config)


db = SQLAlchemy()
socketio = SocketIO()
celery = FlaskCelery()