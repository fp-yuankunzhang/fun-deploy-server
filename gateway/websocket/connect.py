from flask_socketio import send
from gateway.extensions import socketio


@socketio.on('connect')
def on_connect():
    send('connected')


@socketio.on('disconnect')
def on_disconnect():
    return