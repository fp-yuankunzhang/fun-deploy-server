from flask import current_app, request
from flask_socketio import send, emit, disconnect, join_room, leave_room, rooms

from gateway.extensions import socketio


@socketio.on("join")
def on_join():
    socketio.emit("join_ok", {"room": "world"})


@socketio.on("leave")
def on_leave():
    socketio.emit("message", {"room": "world"})