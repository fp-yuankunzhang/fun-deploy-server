# -*- coding: utf-8 -*-
import os

_basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):

    DEBUG = True
    ADMINS = frozenset(['yuankun.zhang@funplus.com'])
    SECRET_KEY = 'modify this in production'

    '''
    SQLAlchemy
    '''
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'development.db')

    '''
    Celery
    '''
    CELERY_ACCEPT_CONTENT = ['json']
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'
    BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
    CELERY_TIMEZONE = 'Asia/Shanghai'
    CELERYBEAT_SCHEDULE = {
    }

    '''
    Websocket
    '''
    SOCKET_IO_PORT = 5001
    SOCKET_IO_PING_INTERVAL = 15
    SOCKET_IO_PING_TIMEOUT = 45
    SOCKETIO_REDIS_URL = 'redis://localhost:6379/0'