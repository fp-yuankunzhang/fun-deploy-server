# Gateway - Deployment API service

## Requirements

* virtualenv 1.11.6+
* pip 1.5.6+

## How to Install

Setup the virtual environment.

```shell
$ virtualenv venv
$ . venv/bin/activate
```

Install project dependencies.

```shell
$ pip install -r requirements.txt
```

Initialize database.

```shell
$ python manage.py db_init
```

Run the debug server.

```shell
$ python runserver.py
```

## Endpoints

* '/' => 'hello'
* '/admin/' => The admin panel